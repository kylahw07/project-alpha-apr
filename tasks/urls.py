from django.urls import path
from tasks.views import create_task, show_mytasks

urlpatterns = [
    path("mine/", show_mytasks, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
