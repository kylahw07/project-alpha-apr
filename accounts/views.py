from django.shortcuts import render, redirect
from accounts.forms import Login, Signup
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout


# Create your views here.
def loginview(request):
    if request.method == "POST":
        form = Login(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = Login()

    context = {
        "form": form,
    }

    return render(request, "accounts/login.html", context)


def logoutview(request):
    logout(request)
    return redirect("login")


def signupview(request):
    if request.method == "POST":
        form = Signup(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )

                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = Signup()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
